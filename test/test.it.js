const chai = require('chai');
const { expect } = require('chai');
const mongoose = require('mongoose');
const app = require('../server');
chai.use(require('chai-http'));

const conn = mongoose.connection;

beforeEach(() => {
  conn.collection('tasks').remove({});

  const task1 = {
    taskId: 1,
    title: 'test task',
    description: 'lorem ipsum',
    color: 'blue',
    priority: 2,
    status: 'in progress',
  };

  const task2 = {
    taskId: 13,
    title: 'test task2',
    description: 'lorem ipsum dolor sit amet',
    color: 'green',
    priority: 3,
    status: 'to do',
  };

  conn.collection('tasks').insert(task1);
  conn.collection('tasks').insert(task2);
});

describe('/GET tasks', () => {

  it('should return all tasks', () => {
    return chai.request(app)
      .get('/tasks')
      .then((res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('array');
      });
  });

  it('should return Not Found', () => {
    return chai.request(app)
      .get('/INVALID_PATH')
      .then(res => expect(res).to.have.status(404));
  });
});

describe('/GET/:id tasks', () => {

  it('should return one task', () => {
    return chai.request(app)
      .get('/tasks/1')
      .then((res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body.taskId).to.be.equal(1);
        expect(res.body.title).to.be.equal('test task');
        expect(res.body.color).to.be.equal('blue');
        expect(res.body.priority).to.be.equal(2);
        expect(res.body.status).to.be.equal('in progress');
      });
  });

  it('should not return task with id that does not exist', () => {
    return chai.request(app)
      .get('/tasks/123')
      .then((res) => {
        expect(res).to.have.status(404);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body).to.have.property('message');
        expect(res.body.message).to.be.equal('Task not found with id 123')
      });
  });
});

describe('/POST Tasks', () => {

  it('should not create task without taskId', () => {
    let task = {
      title: "test task",
      description: "adfa",
      color: "blue",
      priority: 3,
      status: "in progress"
    };
    chai.request(app)
      .post('/tasks')
      .send(task)
      .end((err, res) => {
        expect(res).to.have.status(500);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body).to.have.property('message');
      });
  });

  it('should not create a task with duplicated taskId', () => {
    let task = {
      taskId: 13,
      title: "test task",
      description: "adfa",
      color: "blue",
      priority: 3,
      status: "in progress"
    };

    chai.request(app)
      .post('/tasks')
      .send(task)
      .end((err, res) => {
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body).to.have.property('message');
      });
  });

  it('should create a new task', () => {
    let task = {
      taskId: 999,
      title: 'test task',
      description: 'lorem ipsum',
      color: 'blue',
      priority: 3,
      status: 'in progress',
    };

    chai.request(app)
      .post('/tasks')
      .send(task)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body.taskId).to.be.equal(999);
        expect(res.body.title).to.be.equal('test task');
        expect(res.body.description).to.be.equal('lorem ipsum');
        expect(res.body.color).to.be.equal('blue');
        expect(res.body.priority).to.be.equal(3);
        expect(res.body.status).to.be.equal('in progress');
      });
  });
});

describe('/PUT/:id task', () => {

  it('should update task color', () => {
    chai.request(app)
      .put('/tasks/1')
      .send({color: "green"})
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body.color).to.be.equal('green')
      });
  });

  it('should not update task with not existing id', () => {
    chai.request(app)
      .put('/tasks/123')
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body).to.have.property('message');
        expect(res.body.message).to.be.equal('Task not found with id 123')
      });
  });
});

describe('/DELETE/:id task', () => {

  it('should delete task', () => {
    chai.request(app)
      .delete('/tasks/13')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body).to.have.property('message');
        expect(res.body.message).to.be.equal('Task deleted successfully!')
      });
  });

  it('should not delete task with not existing id', () => {
    chai.request(app)
      .delete('/tasks/123')
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body).to.have.property('message');
        expect(res.body.message).to.be.equal('Task not found with id 123')
      });
  });
});
