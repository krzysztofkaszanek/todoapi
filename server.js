const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dbConfig = require('./config/database.config.js');
require('dotenv').load();

const app = express();
const dbUrl = dbConfig.url;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

mongoose.Promise = global.Promise;

mongoose.connect(dbUrl, { useNewUrlParser: true })
  .then(() => {
    console.log('Successfully connected to the database');
  }).catch(() => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
  });

app.get('/', (req, res) => {
  res.json({ message: 'Welcome to ToDo application.' });
});

require('./app/routes/task.routes.js')(app);

app.listen(3000, () => {
  console.log('Server is listening on port 3000');
});

module.exports = app;
