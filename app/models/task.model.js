const mongoose = require('mongoose');

const TaskSchema = mongoose.Schema({
  taskId: {
    type: Number,
    required: true,
    unique: true,
    autoIncrement: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  status: {
    type: String,
    required: true,
  },
  priority: {
    type: Number,
  },
  color: {
    type: String,
  },
}, {
  timestamps: false,
});

module.exports = mongoose.model('Task', TaskSchema);
