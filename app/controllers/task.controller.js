const Task = require('../models/task.model.js');

exports.create = (req, res) => {
  if (!req.body.title) {
    return res.status(400).send({
      message: 'Task title cannot be empty',
    });
  }

  const task = new Task({
    taskId: req.body.taskId,
    title: req.body.title || 'Untitled Task',
    description: req.body.description || '',
    status: req.body.status || 'To Do',
    priority: req.body.priority,
    color: req.body.color,
  });

  task.save()
    .then(data => res.send(data))
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Task.',
      });
    });
};

exports.findAll = (req, res) => {
  Task.find()
    .then(tasks => res.send(tasks))
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving tasks.',
      });
    });
};

exports.findByTitle = (req, res) => {
  Task.find({ title: req.params.taskTitle })
    .then((task) => {
      if (!task) {
        return res.status(404).send({
          message: 'Task not found with id ' + req.params.taskId,
        });
      }
      res.send(task);
    }).catch((err) => {
    if (err.kind === 'ObjectId') {
      return res.status(404).send({
        message: 'Task not found with id ' + req.params.taskId,
      });
    }
    return res.status(500).send({
      message: 'Error retrieving task with id ' + req.params.taskId,
    });
  });
};

exports.findOne = (req, res) => {
  Task.findOne({ taskId: req.params.taskId })
    .then((task) => {
      if (!task) {
        return res.status(404).send({
          message: 'Task not found with id ' + req.params.taskId,
        });
      }
      res.send(task);
    }).catch((err) => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Task not found with id ' + req.params.taskId,
        });
      }
      return res.status(500).send({
        message: 'Error retrieving task with id ' + req.params.taskId,
      });
    });
};

exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: 'Task can not be empty',
    });
  }

  const update = {};
  if (req.body.title){
    update.title = req.body.title
  }
  if (req.body.description){
    update.description = req.body.description
  }
  if (req.body.status){
    update.status = req.body.status
  }
  if (req.body.priority){
    update.priority = req.body.priority
  }
  if (req.body.color){
    update.color = req.body.color
  }


  Task.findOneAndUpdate({ taskId: req.params.taskId }, update, { new: true })
    .then((task) => {
      if (!task) {
        return res.status(404).send({
          message: 'Task not found with id ' + req.params.taskId,
        });
      }
      res.send(task);
    }).catch((err) => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Task not found with id ' + req.params.taskId,
        });
      }
      return res.status(500).send({
        message: 'Error updating task with id ' + req.params.taskId,
      });
    });
};

exports.delete = (req, res) => {
  Task.findOneAndRemove({taskId: req.params.taskId})
    .then((task) => {
      if (!task) {
        return res.status(404).send({
          message: 'Task not found with id ' + req.params.taskId,
        });
      }
      res.send({ message: 'Task deleted successfully!' });
    }).catch((err) => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Task not found with id ' + req.params.taskId,
        });
      }
      return res.status(500).send({
        message: 'Could not delete task with id ' + req.params.taskId,
      });
    });
};
